4.	Coding exercise: You are tasked to write a checker that validates the parentheses of a LISP code.  Write a program (in Java or JavaScript) which takes in a string as an input and returns true if all the parentheses in the string are properly closed and nested.
        
        
        => please run com.availity.assignment.Question4LISPCheckerSolution1.java
        => please run com.availity.assignment.Question4LISPCheckerSolution2.java
       Please note input file containing the LISP code is in /availity/src/main/resources/LISP.txt


5.	Coding exercise:  Availity receives enrollment files from various benefits management and enrollment solutions (I.e. HR platforms, payroll platforms).  Most of these files are typically in EDI format.  However, there are some files in CSV format.  For the files in CSV format, write a program in a language that makes sense to you that will read the content of the file and separate enrollees by insurance company in its own file. Additionally, sort the contents of each file by last and first name (ascending).  Lastly, if there are duplicate User Ids for the same Insurance Company, then only the record with the highest version should be included. The following data points are included in the file:
       => Please run com.availity.enrollment.handler.EnrollmentHandler.java
       Output file will be generated under root folder /availity.
       Please note input CSV file is present inside /availity/src/main/resources/enrollment.csv



Please run the project initially using mvn clean install command and then run individual class as mentioned above.
If ClassNotFoundException is encountered then please do Project -> clean  and re-run the above mentioned java class.

This project uses opencsv library to write into CSV file as it handles underlying complexity of writing to csv file.


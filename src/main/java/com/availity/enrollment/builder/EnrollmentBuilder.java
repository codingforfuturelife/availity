package com.availity.enrollment.builder;

import com.availity.enrollment.model.Enrollee;

/*
 * This class is to form Enrollment Object while reading the records from CSV file.
 * 
 * @author Chetan Yeole
 */
public class EnrollmentBuilder {
	private final static int USERID = 0;
	private final static int FIRST_NAME = 1;
	private final static int LAST_NAME = 2;
	private final static int VERSION = 3;
	private final static int COMPANY = 4;

	/**
	 * Returns the Enrolle object while reading records from CSV file of individuals
	 * 
	 * @param enrolle
	 *            String array from CSV file.
	 * @return Enrolle Object
	 */
	public static Enrollee getEnrolleObject(String[] enrolle) {
		Enrollee enroll = new Enrollee();
		enroll.setFirstName(enrolle[FIRST_NAME]);
		enroll.setLastName(enrolle[LAST_NAME]);
		enroll.setUserId(enrolle[USERID]);
		enroll.setInsuranceCompany(enrolle[COMPANY]);
		enroll.setVersion(Integer.parseInt(enrolle[VERSION]));
		return enroll;

	}

}

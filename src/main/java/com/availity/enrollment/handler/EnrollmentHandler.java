package com.availity.enrollment.handler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.availity.enrollment.builder.EnrollmentBuilder;
import com.availity.enrollment.model.Enrollee;
import com.availity.file.utility.ReadResouceFile;
import com.opencsv.CSVWriter;


/*
 * 
 *This is a EnrollmentHandler class which reads the enrollment file, read the records, sorts them by first and last name, 
 *saves them by insurance cmopany name in the map and write in individuals insurance company file.
 *
 * @author Chetan Yeole
 * 
 */

public class EnrollmentHandler {

	public static void main(String[] args) {
		EnrollmentHandler handler = new EnrollmentHandler();
		try {

			// Step 1: Read the enrollmentFile
			BufferedReader enrollmentToProcess = handler.enrollmentReader("enrollment.csv");
			// Step 2. Process the enrollmentFiles
			HashMap<String, Object> enrolleTable = handler.enrollmentProcessor(enrollmentToProcess);
			// Step 2. Write the processed enrollment records to file.
			boolean result = handler.enrollmentWriter(enrolleTable);
			if (result) {
				System.out.println("Enrollment has been successfully processed. Please refresh and check under the rool folder availity");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * This method extracts the records of the users from the CSV file and constructs them in object format.
	 * Then put them into HashMap with insurance company name as key and list of user belonging to it as a list.
	 * @param bufferReader
	 * @return hashMap
	 */
	public HashMap<String, Object> enrollmentProcessor(BufferedReader br) throws IOException {
		String line;
		String splitBy = ",";
		HashMap<String, Object> enrolleeTable = new HashMap<>();
		while ((line = br.readLine()) != null) // returns a Boolean value
		{
			String[] enrolle = line.split(splitBy); // use comma as separator
			Enrollee enroll = EnrollmentBuilder.getEnrolleObject(enrolle); // buildEnrolle(enrolle);

			if (enrolleeTable.containsKey(enroll.getInsuranceCompany())) {
				updateEnrolle(enrolleeTable, enroll);
			} else {
				addEnrolle(enrolleeTable, enroll);

			}

		}
		return enrolleeTable;
	}

	/**
	 * This method keeps the map of insurance company and users enrolled into it.
	 * 
	 * @param enrolleeTable
	 * @param enrollee
	 */
	private void addEnrolle(HashMap<String, Object> enrolleeTable, Enrollee enroll) {
		ArrayList<Enrollee> enrolist = new ArrayList<>();
		enrolist.add(enroll);
		enrolleeTable.put(enroll.getInsuranceCompany(), enrolist);
	}

	/**
	 * This method delegates the task of writing records to file.
	 * 
	 * @param enrolleeTable
	 * @return boolean
	 */
	public boolean enrollmentWriter(HashMap<String, Object> enrolleeTable) {
		boolean result = true;
		try {
			enrollmentFileGenerator(enrolleeTable);
		} catch (IOException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}

	private void enrollmentFileGenerator(HashMap<String, Object> enrolleeTable) throws IOException {
		for (Map.Entry<String, Object> set : enrolleeTable.entrySet()) {
			@SuppressWarnings("unchecked")
			ArrayList<Enrollee> enrolist = (ArrayList<Enrollee>) set.getValue();
			File file = new File(set.getKey());
			FileWriter outputfile = new FileWriter(file);
			CSVWriter csvWriter = new CSVWriter(outputfile, '|', CSVWriter.NO_QUOTE_CHARACTER,
					CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
			List<Enrollee> emps = enrolist;
			List<String[]> data = toStringArray(emps);
			csvWriter.writeAll(data);
			csvWriter.close();

		}
	}

	/**
	 * This method keeps the map of insurance company and users enrolled into it.
	 * 
	 * @param enrolleeTable
	 * @param enrollee
	 */
	private void updateEnrolle(HashMap<String, Object> enrolleeTable, Enrollee enroll) {
		@SuppressWarnings("unchecked")
		ArrayList<Enrollee> enrolist = (ArrayList<Enrollee>) enrolleeTable.get(enroll.getInsuranceCompany());
		Enrollee enrollexisting = enrolist.stream().filter(e -> e.getUserId().equalsIgnoreCase(e.getUserId())).findAny()
				.get();
		// if users with same userId exists then Version is compared and higher one is
		// retained.
		if ((enrollexisting.getVersion() < enroll.getVersion())) {
			enrolist.add(enroll);
			enrolist.remove(enrollexisting);
		} else { // Else treated as a new entry in the table.
			enrolist.add(enroll);
		}
		// Arrange the enrolle in the ascending order with their firstName and lastName.
		Collections.sort(enrolist, Comparator.comparing(Enrollee::getFirstName).thenComparing(Enrollee::getLastName));
		enrolleeTable.put(enroll.getInsuranceCompany(), enrolist);
	}

	private  List<String[]> toStringArray(List<Enrollee> emps) {
		List<String[]> records = new ArrayList<String[]>();
		// adding header record
		records.add(new String[] { "ID", "FirstNAme", "LastName", "Version", "Company" });
		Iterator<Enrollee> it = emps.iterator();
		while (it.hasNext()) {
			Enrollee emp = it.next();
			records.add(new String[] { emp.getUserId(), emp.getFirstName(), emp.getLastName(),
					String.valueOf(emp.getVersion()), emp.getInsuranceCompany() });
		}
		return records;
	}

	/**
	 * This methods reads the enrollment files.
	 * 
	 * @param enrollmentFileName
	 * @return enrollmentFileBuffer
	 */
	public BufferedReader enrollmentReader(String enrollmentFile)
			throws FileNotFoundException, UnsupportedEncodingException {
		System.out.println("###### Reading enrollment from availity/src/main/resources ########");
		return new ReadResouceFile().getBufferReader(enrollmentFile);

	}

}

package com.availity.assignment;

import java.util.ArrayDeque;
import java.util.Deque;

import com.availity.file.utility.ReadResouceFile;

/*
 * 
 * Solution for Coding exercise: 
 * You are tasked to write a checker that validates the parentheses of a LISP code. 
 * Write a program (in Java or JavaScript) which takes in a string as an input and 
 * returns true if all the parentheses in the string are properly closed and nested. 
 *  
 * Approach2: Without using Stream API, we use use for loop to loop through the contents of the contents at 
 *  at Character level and use Double ended queue to store parenthesis.
 * 
 * @author Chetan Yeole
 * @since   2021-09-19
 */
public class Question4LISPCheckerSolution2 {

	/**
	 * This method checks if the LISP snippet has balanced set of parenthesis.
	 * 
	 * @return Boolean
	 */
	public static boolean isBraceBalanced(String braces) {
		Deque<Character> queue = new ArrayDeque<Character>();

		for (char c : braces.toCharArray()) {
			if (c == '(') {
				queue.push(c);
			} else if ((c == ')' && (queue.isEmpty() || queue.pop() != '(')))

			{
				return false;
			}
		}

		return queue.isEmpty();
	}

	public static void main(String args[]) {
		System.out.println("#######Is LISP CODE Balanced #########");
		System.out.println(isBraceBalanced(new ReadResouceFile().readFile("LISP.txt")));

	}

}

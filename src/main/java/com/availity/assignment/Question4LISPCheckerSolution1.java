package com.availity.assignment;

import com.availity.file.utility.ReadResouceFile;

/*
 * 
 * Solution for Coding exercise: 
 * You are tasked to write a checker that validates the parentheses of a LISP code. 
 * Write a program (in Java or JavaScript) which takes in a string as an input and 
 * returns true if all the parentheses in the string are properly closed and nested. 
 *  
 * Approach1: Below Implementation makes use of Java Stream API
 * 
 * @author Chetan Yeole
 * @since   2021-09-19
 */
public class Question4LISPCheckerSolution1 {

	/**
	 * This method checks if the LISP snippet has balanced set of parenthesis.
	 * 
	 * @return Boolean
	 */
	public static boolean checkifBalancedParenthesis() {
		boolean ifBalanced = false;
		String content = new ReadResouceFile().readFile("LISP.txt");
		long countofLeft = content.chars().filter(c -> c == '(').count();
		long countofRight = content.chars().filter(c -> c == ')').count();
		if (countofLeft == countofRight) {
			ifBalanced = true;
		}
		return ifBalanced;
	}

	public static void main(String args[])
	{
		boolean ifBalanced = checkifBalancedParenthesis();
		System.out.println("######### Is LISP Code Balanced ########");
		System.out.println(ifBalanced);
	}
}

package com.availity.assignment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class LISPCheckerUtil {
	/**
	 * This method read the content of the file containing LISP code snippet
	 * @return String
	 */
	public static String readFileContents(String fileName) {
		String data = "";
		try {
			File myObj = new File("../avality/".concat(fileName));
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				data = data.concat(myReader.nextLine());
				
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		System.out.println("###Contents of File###"); // SOP left deliberately to display contents on the console.
		System.out.println(data);
		return data;
	}
	
	public  String readFile(String fileName) {
    	StringBuilder sb = new StringBuilder();
        try {
            
            BufferedReader br = getBufferReader(fileName);
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + System.lineSeparator());
            }
			
			System.out.println("contents*** "+sb.toString());
			br.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

	public BufferedReader getBufferReader(String fileName) throws UnsupportedEncodingException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
		        this.getClass().getResourceAsStream("/" + fileName), "UTF-8"));
		return br;
	}

	
}

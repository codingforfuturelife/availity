package com.availity.file.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;


public class ReadResouceFile {

    public  String readFile(String fileName) {
    	StringBuilder sb = new StringBuilder();
        try {
            
            BufferedReader br = getBufferReader(fileName);
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + System.lineSeparator());
            }
			
			br.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    /*
     *  This doesn't allow to be static method.
     */
	public  BufferedReader getBufferReader(String fileName) throws UnsupportedEncodingException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
		        this.getClass().getResourceAsStream("/" + fileName), "UTF-8"));
		return br;
	}

    public static void main(String[] args) {
        ReadResouceFile readResouceFile = new ReadResouceFile();
        readResouceFile.readFile("LISP.txt");
    }

}